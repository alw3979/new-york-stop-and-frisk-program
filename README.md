# New York Stop and Frisk Program
This project was submitted as a collaborative group project for Fall 2019 CS 329E Data Analytics. 
Group members are: Anna Williams, Zizhuo (Xavier) Dong, Nyle Ashraf, Alexandra Smith.
This repository was not necessary for the project but created after the submission of the project. 
The development of the jupyter-notebook in the files was made possible through Google Colab, atleast 
until we suprassed our alloted disc space.

All files (2003-2012) are from [enigma](https://public.enigma.com/datasets/new-york-city-police-departments-stop-and-frisk-program-2003/151b7325-1fd3-4785-bbbf-1301a6742820)